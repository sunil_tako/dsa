/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package studymonitorinterface.java;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 *
 * @author Lenovo
 */
public class DatabaseUtility {

    private final String jdbcDriver = "com.mysql.jdbc.Driver";
    private final String dbAddress = "jdbc:mysql://localhost:3307/";
    private final String userPass = "?useSSL=false&user=root&password=root";
    private final String dbName = "studentmonitor";
    private static Connection con;

    public DatabaseUtility() {

    }

    public void createDatabase() {
        try {
            System.out.println("To create database");
            Class.forName(jdbcDriver);
            con = DriverManager.getConnection(dbAddress + userPass);
            System.out.println(dbAddress + userPass);
            Statement s = con.createStatement();
            int myResult = s.executeUpdate("CREATE DATABASE IF NOT EXISTS " + dbName);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public void createTables() {

        try {

            System.out.println("Connected database successfully...");
            con = DriverManager.getConnection(dbAddress + dbName + userPass);
            Statement stmt = con.createStatement();
            System.out.println(dbAddress + dbName + userPass);
            System.out.println("Creating student Table");

            /*
            Creating Student table
             */
            String studentTable = "CREATE TABLE IF NOT EXISTS STUDENT (STUDENTID INTEGER NOT NULL, STUDENTNAME VARCHAR(30), YEARLEVEL VARCHAR(2), PRIMARY KEY(STUDENTID))";
            stmt.executeUpdate(studentTable);
            System.out.println("Creating subject Table");

            /*
            Creating subject table
            
             */
            String subjectTable = "CREATE TABLE IF NOT EXISTS SUBJECT (SUBJECTID INTEGER NOT NULL, SUBJECTNAME VARCHAR(50),  PRIMARY KEY(SUBJECTID))";
            stmt.executeUpdate(subjectTable);
            System.out.println("Creating assessment Table");

            /*
            Creating table Assessment
             */
            String assessmentTable = "CREATE TABLE IF NOT EXISTS ASSESSMENT (ID INTEGER NOT NULL,SUBJECT VARCHAR(100), ASSESSMENTID VARCHAR(10), ASSESSMENTTYPE VARCHAR(100) , TOPIC VARCHAR(100) , FORMAT VARCHAR(100) , DUEDATE  VARCHAR(100)  , PRIMARY KEY (ID) ) ";
            stmt.executeUpdate(assessmentTable);

            /*
            Creating table grade
             */
            String gradeTable = "CREATE TABLE IF NOT EXISTS GRADE ( DEGREEOFACHIEVEMENT VARCHAR(15) , DEGREEOFKNOWLEDGEANDUNDERSTANDING VARCHAR(100) , DEGREEOFSKILLANDUSEOFSKILL VARCHAR(100) , PRIMARY KEY (DEGREEOFACHIEVEMENT) ) ";
            stmt.executeUpdate(gradeTable);
            System.out.println("Creating student grade Table");

            /*
            Creating table STUDENTGRADE
             */
            String studentGrade = "CREATE TABLE IF NOT EXISTS STUDENTGRADE ( STUDENTID INTEGER , SUBJECTID INTEGER , ASSESSMENTID INTEGER , DEGREEOFACHIEVEMENT VARCHAR(15) , PRIMARY KEY( STUDENTID,SUBJECTID,ASSESSMENTID,DEGREEOFACHIEVEMENT) ) ";
            stmt.executeUpdate(studentGrade);

            System.out.println("Created all the table...");

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

    }

    public void insertSubjectData(ArrayList<Subject> subjectList) {

        try {

            String query = "INSERT INTO SUBJECT (subjectId, subjectname) ";
            String tempSql = "";
            ArrayList<Subject> subjects = subjectList;
            int count = 0;
            int subjectCounter = 1;
            for (Subject subject : subjects) {
                subject.setSubjectId(subjectCounter);
                tempSql += " SELECT " + subjectCounter + ",'" + subject.getName() + "'";
                count++;
                if (count < subjects.size()) {
                    tempSql += " UNION All ";
                }
                subjectCounter++;
            }
            query += tempSql;
            System.out.println(query);
            PreparedStatement preparedStmt = con.prepareStatement(query);

            preparedStmt.execute();

        } catch (SQLException ex) {
            // Logger.getLogger(DatabaseUtility.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getMessage());
        }

    }

    public void insertAssessmentsData(ArrayList<Subject> subjectList) {

        try {
            System.out.println("Preparing to insert assessment");
            String query = "INSERT INTO ASSESSMENT(ID,SUBJECT,ASSESSMENTID, ASSESSMENTTYPE,TOPIC,FORMAT,DUEDATE)";
            int id = 1;
            for (int i = 0; i < subjectList.size(); i++) {
                String name = subjectList.get(i).getName();
                subjectList.get(i).getAssessments().setId(id);
                String assessmentId = subjectList.get(i).getAssessments().getAssessmentId();
                String type = subjectList.get(i).getAssessments().getAssessmentType();
                String topic = subjectList.get(i).getAssessments().getTopic();
                String format = subjectList.get(i).getAssessments().getFormat();
                String dueDate = subjectList.get(i).getAssessments().getDueDate();
                String tempSql = "SELECT " + id + ",'" + name + "','" + assessmentId + "','" + type + "','" + topic + "','" + format + "','" + dueDate + "'";
                id++;
                if (id < subjectList.size()) {
                    tempSql += " UNION All ";
                }
                query += tempSql;

            }
            System.out.println("Preparing to insert assessment");
            System.out.println(query);
            Statement m = con.createStatement();
            m.executeUpdate(query);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public void insertGradeData() {
        try {
            System.out.println("inserting grade data");
            con = DriverManager.getConnection(dbAddress + dbName + userPass);
            ResultSet resultSet;
            String queryMax = "SELECT COUNT(*) AS TOTAL FROM GRADE";
            Statement m = con.createStatement();
            resultSet = m.executeQuery(queryMax);
            int recordCount = 0;
            while (resultSet.next()) {
                recordCount = resultSet.getInt("TOTAL");
            }
            System.out.println(recordCount);
            if (recordCount == 0) {
                System.out.println("Preparing insert script");
                Statement stmt = con.createStatement();
                String gradeTableSql = "INSERT INTO GRADE(DEGREEOFACHIEVEMENT,DEGREEOFKNOWLEDGEANDUNDERSTANDING,DEGREEOFSKILLANDUSEOFSKILL)\n"
                        + "SELECT 'Very high', 'thorough understanding','uses a high level of skill in both familiar and new situations' UNION ALL \n"
                        + "SELECT 'High', 'clear understanding','uses a high level of skill in familiar situations, and is beginning to use skills in new situations' UNION ALL \n"
                        + "SELECT 'Sound', 'understanding','uses skills in situations familiar to them ' UNION ALL \n"
                        + "SELECT 'Developing', 'understands aspects of',' uses varying levels of skill in situations familiar to them ' UNION ALL \n"
                        + "SELECT 'Emerging', 'basic understanding','beginning to use skills in familiar situations'";
                stmt.executeUpdate(gradeTableSql);
                System.out.println("Inserted successfully");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public String saveDataIntoDatabase(LinkedList<Student> studentList, ArrayList<Subject> subjectList) {

        try {
            con = DriverManager.getConnection(dbAddress + dbName + userPass);

            for (Student student : studentList) {
                /*
                Saving student records
                 */
                saveStudentAndAssessementGrades(student, subjectList);

            }

        } catch (SQLException ex) {
        }

        return "";
    }

    private void saveStudentAndAssessementGrades(Student student, ArrayList<Subject> subjectList) {

        try {

            String queryMax = " select max(studentid) as StudentId from student";
            Statement m = con.createStatement();
            ResultSet r1 = m.executeQuery(queryMax);

            int studentid = 0;
            while (r1.next()) {
                studentid = r1.getInt("StudentId");
                studentid++;
            }

            String query = "INSERT INTO STUDENT (studentid, studentName, yearLevel) VALUES" + "(?,?,?)";
            PreparedStatement preparedStmt = con.prepareStatement(query);
            preparedStmt.setInt(1, studentid);
            preparedStmt.setString(2, student.getName());
            preparedStmt.setString(3, student.getYear());

            preparedStmt.executeUpdate();

            /*
            Saving grade of student
             */
            for (Subject subject : subjectList) {

                MarkedAssessment assessment = subject.getAssessments();

                int studentId = studentid;
                int subjectId = subject.getSubjectId();
                int assessmentId = subject.getAssessments().getId();
                String degreeOfAchievement = assessment.getGrade();

                query = "INSERT INTO STUDENTGRADE (STUDENTID, SUBJECTID, ASSESSMENTID, DEGREEOFACHIEVEMENT) VALUES" + "(?,?,?,?)";
                preparedStmt = con.prepareStatement(query);
                preparedStmt.setInt(1, studentId);
                preparedStmt.setInt(2, subjectId);
                preparedStmt.setInt(3, assessmentId);
                preparedStmt.setString(4, degreeOfAchievement);

                preparedStmt.executeUpdate();

            }

        } catch (SQLException ex) {
            // Logger.getLogger(DatabaseUtility.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getMessage());
        }

    }

}
