/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package studymonitorinterface.java;

import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class Subject {

    private int subjectId;
    private String Name;
    private MarkedAssessment Assessments;

    /**
     * @return the Name
     */
    public String getName() {
        return Name;
    }

    /**
     * @param Name the Name to set
     */
    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     * @return the Assessments
     */
    public MarkedAssessment getAssessments() {
        return Assessments;
    }

    /**
     * @param Assessments the Assessments to set
     */
    public void setAssessments(MarkedAssessment Assessments) {
        this.Assessments = Assessments;
    }

    /**
     * @return the subjectId
     */
    public int getSubjectId() {
        return subjectId;
    }

    /**
     * @param subjectId the subjectId to set
     */
    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

}
