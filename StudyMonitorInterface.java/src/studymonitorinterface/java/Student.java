/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package studymonitorinterface.java;

import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class Student {

    private int Id;
    private String Name;
    private String Year;
    private ArrayList<Subject> Subjects;
    private ArrayList<Subject> subjectList;

    /**
     * @return the Id
     */
    public int getId() {
        return Id;
    }

    /**
     * @param Id the Id to set
     */
    public void setId(int Id) {
        this.Id = Id;
    }

    /**
     * @return the Year
     */
    public String getYear() {
        return Year;
    }

    /**
     * To set the year of enrollment
     */
    public void setYear(String Year) {
        this.Year = Year;
    }

    /**
     * To get the Subject list enrolled by the student
     */
    public ArrayList<Subject> getSubjects() {
        return Subjects;
    }

    /**
     * @param Subjects the Subjects to set
     */
    public void setSubjects(ArrayList<Subject> subjects) {
        this.Subjects = subjects;
    }

    /**
     * @return the Name
     */
    public String getName() {
        return Name;
    }

    /**
     * @param Name the Name to set
     */
    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     * @return the subjectList
     */
    public ArrayList<Subject> getSubjectList() {
        return subjectList;
    }

    /**
     * @param subjectList the subjectList to set
     */
    public void setSubjectList(ArrayList<Subject> subjectList) {
        this.subjectList = subjectList;
    }

//use the Subject class's toString() method to display the subjects.
    @Override
    public String toString() {
        return String.format("\n%s  %s\n", this.Name, this.Year);
    }

}
