package studymonitorinterface.java;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Scanner;
import java.util.List;

/**
 * @author Raju Sharma Chapagain(12033952)
 * @author Sunil Tako
 * @createdDate 16 April 2017 Second version enhancement 21 May 2017
 * @Subject: Data Structure and Algorithms
 */
@SuppressWarnings("unchecked")
public class StudyMonitorInterfaceJava extends JFrame {

    //List of panels required
    JPanel mainPanel;

    //Panel to display contents at the top 
    JPanel topPanel;
    JPanel mainPanelRow1;
    JPanel mainPanelRow2;
    JPanel mainPanelRow3;

    //Panel for displaying textbox and mid panel border
    JPanel midPanel;
    JPanel MessagePanel;

    //panel to display the action buttons
    JPanel bottomPanel;

    //List of Labels to be generated
    JLabel lblStudent;
    JLabel lblYear;
    JLabel lblSubject;
    JLabel lblAssessments;
    JLabel lblAchievements;
    JLabel lblAdminName;
    JLabel lblPassword;

    //List of text fields to be generated
    JTextField txtStudentName;
    JTextField txtYear;
    JTextField txtAdminName;
    JTextField txtPassword;

    //List of combo box to be generated
    JComboBox cbSubjects;
    JComboBox cbAssessments;
    JComboBox cbAchievements;

    //List of text area to be generated
    JTextArea messageArea;

    //List of buttons to be generated
    JButton btnCreateStudent;
    JButton btnLoadAssessment;
    JButton btnSave;
    JButton btnDisplayAssessment;
    JButton btnSetGrade;
    JButton btnDisplayGrade;
    JButton btnClearDisplay;
    JButton btnExit;
    JButton btnLogin;

    //Set static variables
    private static final int FRAME_HEIGHT = 1250;
    private static final int FRAME_WIDTH = 750;
    private static final int ADMIN_TEXT_FIELD_WIDTH = 15;
    private static final int PASSWORD_TEXT_FIELD_WIDTH = 10;
    private static final int STUDENT_TEXT_FIELD_WIDTH = 30;
    private static final int YEAR_TEXT_FIELD_WIDTH = 4;
    private static final int TEXT_AREA_HEIGHT = 30;
    private static final int TEXT_AREA_WIDTH = 70;
    private static final int TOP_PANEL_GRID_Y = 3;
    private static final int TOP_PANEL_GRID_X = 1;
    private static final String ADMIN_LABEL_TEXT = "Admin Name";
    private static final String PASSWORD_LABEL_TEXT = "Password";
    private static final String USERNAME = "admin";
    private static final String PASSWORD = "admin";
    private static final String LOGIN_BUTTON_TEXT = "Login";
    private static final String STUDENT_LABEL_TEXT = "Student Name";
    private static final String YEAR_LABEL_TEXT = "Year Level";
    private static final String SUBJECT_LABEL_TEXT = "Subjects";
    private static final String ASSESSMENT_LABEL_TEXT = "Assessments";
    private static final String ACHIEVEMENT_LABEL_TEXT = "Achievements";
    private static final String CREATE_BUTTON_TEXT = "Create Student";
    private static final String LOAD_ASSESSMETNT_BUTTON_TEXT = "Load Assessment";
    private static final String SAVE_BUTTON_TEXT = "Save";
    private static final String DISPLAY_ASSESSMENT_BUTTON_TEXT = "Display Assessment";
    private static final String SET_GRADE_BUTTON_TEXT = "Set Grade";
    private static final String DISPLAY_GRADE_BUTTON_TEXT = "Display Grade";
    private static final String CLEAR_DISPLAY_BOTTON_TEXT = "Clear Display";
    private static final String EXIT_BUTTON_TEXT = "Exit";
    private static final String TOP_PANEL_BORDER_TEXT = "Student Progress Monitor";
    private static final String MID_PANEL_BORDER_TEXT = "Student Performance";
    private static final String BOTTOM_PANEL_BORDER_TEXT = "Command Buttons";
    private static final String CB_SUBJECT_DEFAULT_TEXT = "Choose Subject";
    private static final String CB_ASSESSMENT_DEFAULT_TEXT = "Choose Assessment";
    private static final String CB_ACHIEVEMENT_DEFAULT_TEXT = "Choose Grade";
    private static final String FILE_PATH = "COIT20256Ass1Data.csv";
    private static final String[] ACHIEVEMENTS = {"Very high", "High", "Sound", "Developing", "Emerging"};
    private static final List<String> SUBJECTS = Arrays.asList("Biology", "Business and Communication Technologies", "English", "Maths B", "Religion and Ethics");

    Student studentObj = new Student();
    LinkedList<Student> studentList = new LinkedList<Student>();
    ArrayList<Subject> subjectList = new ArrayList<Subject>();
    Map<String, String> knowledgeMap = new HashMap<>();
    Map<String, String> skillMap = new HashMap<>();
    private static int assessmentCounter = 0;
    public boolean assessmentDataUpdateStatus = false;

    //Main constructor
    public StudyMonitorInterfaceJava() {
        //Set frame size and location
        setSize(FRAME_HEIGHT, FRAME_WIDTH);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int screenWidth = screenSize.width;
        int screenHeight = screenSize.height;
        int x = (screenWidth - getWidth()) / 2;
        int y = (screenHeight - getHeight()) / 2;
        setLocation(x, y);

        //Set frame size fixed
        setResizable(false);
        //Set action for close operation
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        //Create display panel
        CreateDisplayPanel();
        //Add display panel into frame
        add(mainPanel);
        //Set visibility of frame
        setVisible(true);
    }

    //Code to design the main display panel
    private void CreateDisplayPanel() {

        //Initialize main panel and its border
        mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        mainPanel.setBorder(BorderFactory.createTitledBorder(TOP_PANEL_BORDER_TEXT));

        //Initialize top panel
        topPanel = new JPanel(new GridLayout(TOP_PANEL_GRID_Y, TOP_PANEL_GRID_X));
        CreateTopPanelComponent(topPanel);

        //Initialize mid panel 
        midPanel = new JPanel(new FlowLayout());
        midPanel.setBorder(BorderFactory.createTitledBorder(MID_PANEL_BORDER_TEXT));
        CreateMidPanelComponent(midPanel);

        //Initialize bottom panel 
        bottomPanel = new JPanel(new GridLayout());
        bottomPanel.setBorder(BorderFactory.createTitledBorder(BOTTOM_PANEL_BORDER_TEXT));
        CreateBottomPanelComponent(bottomPanel);

        //Add all three main panels into main panel 
        mainPanel.add(topPanel, BorderLayout.NORTH);
        mainPanel.add(midPanel);
        mainPanel.add(bottomPanel, BorderLayout.SOUTH);

        InitializeMapTableForGrade();
        InitializeEventActionListener();
    }

    //Create top panel components of the form
    private void CreateTopPanelComponent(JPanel topPanel) {
        //Create labels as required in top panel 
        lblStudent = new JLabel(STUDENT_LABEL_TEXT);
        lblYear = new JLabel(YEAR_LABEL_TEXT);
        lblSubject = new JLabel(SUBJECT_LABEL_TEXT);
        lblAssessments = new JLabel(ASSESSMENT_LABEL_TEXT);
        lblAchievements = new JLabel(ACHIEVEMENT_LABEL_TEXT);
        lblAdminName = new JLabel(ADMIN_LABEL_TEXT);
        lblPassword = new JLabel(PASSWORD_LABEL_TEXT);

        //List of text fields to be generated
        txtStudentName = new JTextField(STUDENT_TEXT_FIELD_WIDTH);
        txtYear = new JTextField(YEAR_TEXT_FIELD_WIDTH);
        txtAdminName = new JTextField(ADMIN_TEXT_FIELD_WIDTH);
        txtPassword = new JPasswordField(PASSWORD_TEXT_FIELD_WIDTH);

        btnLogin = new JButton(LOGIN_BUTTON_TEXT);

        //List of combo box to be generated
        cbSubjects = new JComboBox();
        cbAssessments = new JComboBox();
        cbAchievements = new JComboBox();
        cbSubjects.addItem(CB_SUBJECT_DEFAULT_TEXT);
        cbAssessments.addItem(CB_ASSESSMENT_DEFAULT_TEXT);
        cbAchievements.addItem(CB_ACHIEVEMENT_DEFAULT_TEXT);
        //Initialize two panel to display components in two rows
        mainPanelRow1 = new JPanel(new FlowLayout());
        mainPanelRow2 = new JPanel(new FlowLayout());
        mainPanelRow3 = new JPanel(new FlowLayout());

        //Add each components in respective row simultaneously
        mainPanelRow1.add(lblAdminName);
        mainPanelRow1.add(txtAdminName);
        mainPanelRow1.add(lblPassword);
        mainPanelRow1.add(txtPassword);
        mainPanelRow1.add(btnLogin);
        mainPanelRow2.add(lblStudent);
        mainPanelRow2.add(txtStudentName);
        mainPanelRow2.add(lblYear);
        mainPanelRow2.add(txtYear);
        mainPanelRow3.add(lblSubject);
        mainPanelRow3.add(cbSubjects);
        mainPanelRow3.add(lblAssessments);
        mainPanelRow3.add(cbAssessments);
        mainPanelRow3.add(lblAchievements);
        mainPanelRow3.add(cbAchievements);

        //Add two row panels inside top panel
        topPanel.add(mainPanelRow1);
        topPanel.add(mainPanelRow2);
        topPanel.add(mainPanelRow3);
    }

    //Create mid panel components
    private void CreateMidPanelComponent(JPanel midPanel) {
        //Initialize the text area
        messageArea = new JTextArea(TEXT_AREA_HEIGHT, TEXT_AREA_WIDTH);
        messageArea.setText("");
        Font font = messageArea.getFont();
        messageArea.setFont(font.deriveFont(Font.BOLD));
        messageArea.setEditable(false);
        //Initialize panel to put the text area
        MessagePanel = new JPanel(new FlowLayout());
        //Add text area into message panel 
        MessagePanel.add(messageArea);
        MessagePanel.setFont(new Font(YEAR_LABEL_TEXT, Font.BOLD, WIDTH));
        //add message panel into mid panel
        midPanel.add(MessagePanel);
    }

    //Create bottom panel components
    private void CreateBottomPanelComponent(JPanel bottomPanel) {

        //Initialize button components as per the requirement
        btnCreateStudent = new JButton(CREATE_BUTTON_TEXT);
        btnCreateStudent.setEnabled(false);
        btnLoadAssessment = new JButton(LOAD_ASSESSMETNT_BUTTON_TEXT);
        btnLoadAssessment.setEnabled(false);
        btnSave = new JButton(SAVE_BUTTON_TEXT);
        btnSave.setEnabled(false);
        btnDisplayAssessment = new JButton(DISPLAY_ASSESSMENT_BUTTON_TEXT);
        btnSetGrade = new JButton(SET_GRADE_BUTTON_TEXT);
        btnSetGrade.setEnabled(false);
        btnDisplayGrade = new JButton(DISPLAY_GRADE_BUTTON_TEXT);
        btnClearDisplay = new JButton(CLEAR_DISPLAY_BOTTON_TEXT);
        btnExit = new JButton(EXIT_BUTTON_TEXT);

        //Add each button components into bottom panel
        bottomPanel.add(btnCreateStudent);
        bottomPanel.add(btnLoadAssessment);
        bottomPanel.add(btnSave);
        bottomPanel.add(btnDisplayAssessment);
        bottomPanel.add(btnSetGrade);
        bottomPanel.add(btnDisplayGrade);
        bottomPanel.add(btnClearDisplay);
        bottomPanel.add(btnExit);

    }

    //Initialize the action of each event of each components
    private void InitializeEventActionListener() {

        btnLogin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Login();
            }
        });
        btnCreateStudent.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CreateStudent();
            }
        });
        btnLoadAssessment.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LoadAssessment();
            }
        });
        btnSave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                save();
            }
        });
        btnDisplayAssessment.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String SubjectName = cbSubjects.getSelectedItem().toString();
                DisplayAssessment(SubjectName);
            }
        });
        btnSetGrade.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SetGrade();
            }
        });
        btnDisplayGrade.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DisplayGrade();
            }
        });
        //To clear the display panel along with object and local variables.
        btnClearDisplay.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ClearDisplay();
            }
        });
        //To exit system on exit button click.
        btnExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Exit();
            }
        });
        //To check the textbox data and making decision to enable the create button
        txtYear.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void changedUpdate(DocumentEvent e) {
                EnableCreate();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                EnableCreate();
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                EnableCreate();
            }
        });
        //To check the textbox data and making decision to enable the create button
        txtStudentName.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void changedUpdate(DocumentEvent e) {
                EnableCreate();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                EnableCreate();
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                EnableCreate();
            }
        });
        cbSubjects.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                //determine whether item is selected
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    String Subject = cbSubjects.getSelectedItem().toString();
                    if (!Subject.isEmpty()) {
                        UpdateAssessments(Subject);
                    }
                }
            }

        });
        //To check if there is any change in selected combo box item. 
        cbAssessments.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                //To determine whether item is selected
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    if (cbAssessments.getSelectedItem() != CB_ASSESSMENT_DEFAULT_TEXT) {
                        CheckGradeSetupStatus();
                    }
                }
            }
        });
    }

    //Method to authenticate the user
    private void Login() {
        try {
            String user = txtAdminName.getText();
            String pass = txtPassword.getText();
            if (user.equals(USERNAME) && pass.equals(PASSWORD)) {
                JOptionPane.showMessageDialog(midPanel, "Login Successful");
                btnCreateStudent.setEnabled(true);
                btnLogin.setEnabled(false);
            } else {
                JOptionPane.showMessageDialog(midPanel, "Invalid login credentials, Please try again");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(midPanel, "System exception encountered");
        }
    }

    //Method to create students for assessment
    private void CreateStudent() {
        try {

            String studentName = "";
            String year = "";
            studentName = txtStudentName.getText();
            year = txtYear.getText();

//            if (studentName.length() > 1 && year.length() == 2 && year.matches("\\d+")) {
//                btnCreateStudent.setEnabled(true);
//                messageArea.setText("You can now add student using the Create Student button");
//            } else if (year.length() != 2) {
//                messageArea.setText("Year should be 2 digit number only");
//                btnCreateStudent.setEnabled(false);
//            } else {
//                btnCreateStudent.setEnabled(false);
//                messageArea.setText("");
//            }
            if ((studentName.length() > 1) && year.length() == 2) {
                //  DatabaseUtility obj = new DatabaseUtility();
                studentObj = new Student();
                studentObj.setName(studentName);
                studentObj.setYear(year);

                ArrayList<Subject> subjects = new ArrayList<Subject>();

                for (String subjectName : SUBJECTS) {
                    Subject subject = new Subject();
                    subject.setName(subjectName);
                    subjects.add(subject);
                }

                studentObj.setSubjects(subjects);
                btnLoadAssessment.setEnabled(true);
                String msg = "Student added to the System. You can now load Assessments using the Load Assessment Button\n";

                studentList.add(studentObj);

                for (Student student : studentList) {

                    msg = msg + student.getName() + " " + student.getYear() + "\n";
                    subjects = student.getSubjects();

                    for (Subject subject : subjects) {

                        msg = msg + subject.getName() + ", ";

                    }

                    msg = msg + "\n";

                }

                messageArea.setText(msg);

                JOptionPane.showMessageDialog(midPanel, "Student created", "Success", 1);
                btnCreateStudent.setEnabled(false);
            } else {
                JOptionPane.showMessageDialog(midPanel, "Please enter valid data to create student", "Failed", 0);
                messageArea.setText("Please enter valid data to create student");
            }
        } catch (HeadlessException ex) {
            btnLoadAssessment.setEnabled(true);
            JOptionPane.showMessageDialog(midPanel, ex.getMessage(), "Failed", 0);
        }
    }

    //Method to load assement of student from file
    private void LoadAssessment() {

        try {
            if (assessmentCounter == 1) {
                JOptionPane.showMessageDialog(midPanel, "Assessment is already loaded");
            } else {
                assessmentCounter = 1;
                Scanner scanner = new Scanner(new File(FILE_PATH));
                while (scanner.hasNextLine()) {

                    //Initialize the subject class to load assessment data from file into object array
                    Subject subject = new Subject();
                    
                    MarkedAssessment markedAssessment = new MarkedAssessment();
                    String[] res = scanner.nextLine().split(",");
                    subject.setName(res[0]);
                    markedAssessment.setGrade("Not Graded");

                    //inherited properties of assessment object
                    markedAssessment.setAssessmentId(res[1]);
                    markedAssessment.setAssessmentType(res[2]);
                    markedAssessment.setTopic(res[3]);
                    markedAssessment.setFormat(res[4]);
                    markedAssessment.setDueDate(res[5]);
                    markedAssessment.setGraded(false);
                    subject.setAssessments(markedAssessment);

                    //add subject to subject list
                    subjectList.add(subject);
                }
                if (!assessmentDataUpdateStatus) {
                    DatabaseUtility obj = new DatabaseUtility();
                    obj.createDatabase();
                    obj.createTables();

                    obj.insertGradeData();
                    obj.insertSubjectData(subjectList);
                    obj.insertAssessmentsData(subjectList);
                    assessmentDataUpdateStatus = true;
                }
                studentObj.setSubjectList(subjectList);
                //populate subjects into combo box
                ArrayList<Subject> res = studentObj.getSubjects();
                for (Subject re : res) {
                    cbSubjects.addItem(re.getName());
                }
                //Populate achievements into combo; 
                for (String achievement : ACHIEVEMENTS) {
                    cbAchievements.addItem(achievement);
                }
                JOptionPane.showMessageDialog(midPanel, "Assessment Loaded successfully");
                messageArea.setText("Assessment Loaded successfully");

                btnSetGrade.setEnabled(true);
            }

        } catch (FileNotFoundException exception) {
            JOptionPane.showMessageDialog(midPanel, exception.getMessage(), "File not found", 0);
        } catch (HeadlessException ex) {
            JOptionPane.showMessageDialog(midPanel, ex, "Alert", 0);
        }
    }

    //Method to display the assement result of student
    private void DisplayAssessment(String SubjectName) {
        messageArea.setText("");
        if (cbSubjects.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(midPanel, "Subject is not selected");
        } else {
            String text = SubjectName;
            for (int i = 0; i < subjectList.size(); i++) {
                String name = subjectList.get(i).getName();
                String id = subjectList.get(i).getAssessments().getAssessmentId();
                String type = subjectList.get(i).getAssessments().getAssessmentType();
                String topic = subjectList.get(i).getAssessments().getTopic();
                String format = subjectList.get(i).getAssessments().getFormat();
                String dueDate = subjectList.get(i).getAssessments().getDueDate();
                boolean isGraded = subjectList.get(i).getAssessments().isGraded();
                String graded = "Not Graded";
                if (isGraded) {
                    graded = "Graded";
                }
                String line = id + "," + type + "," + topic + "," + format + "," + dueDate + "," + graded;
                if (name.toUpperCase().equals(SubjectName.toUpperCase())) {
                    text += "\n" + line;
                }
                messageArea.setText(text);
            }
        }
    }

    //Method to set grade of student
    private void SetGrade() {
        if (cbSubjects.getSelectedIndex() > 0 && cbAchievements.getSelectedIndex() > 0) {

            String assessments = cbAssessments.getSelectedItem().toString();
            String grade = cbAchievements.getSelectedItem().toString();
            String[] res = assessments.split(",");
            String subjectName = cbSubjects.getSelectedItem().toString();
            String assessmentId = res[1];
            boolean found = false;
            //To set grade on each subject
            for (int i = 0; i < subjectList.size(); i++) {
                String name = subjectList.get(i).getName();
                String id = subjectList.get(i).getAssessments().getAssessmentId();
                if ((subjectName.toUpperCase().equals(name.toUpperCase())) && assessmentId.equals(id)) {
                    subjectList.get(i).getAssessments().setGrade(grade);
                    subjectList.get(i).getAssessments().setGraded(true);
                    found = true;
                    break;
                }
            }
            if (found) {
                messageArea.setText("Grade setup successful");
                btnSave.setEnabled(true);
            } else {
                JOptionPane.showMessageDialog(midPanel, "Updated failed", "Failed", 0);
            }

        } else {
            JOptionPane.showMessageDialog(midPanel, "Please select the grade from combo box");
        }
    }

    //Method to display grade of student
    private void DisplayGrade() {
        try {
            messageArea.setText("");
            String assessments = cbAssessments.getSelectedItem().toString();
            String[] res = assessments.split(",");
            String subjectName = res[0];
            String assessmentId = res[1];
            //Generage the information 
            for (int i = 0; i < subjectList.size(); i++) {
                String name = subjectList.get(i).getName();
                String id = subjectList.get(i).getAssessments().getAssessmentId();
                if ((subjectName.toUpperCase().equals(name.toUpperCase())) && assessmentId.equals(id)) {
                    boolean isGraded = subjectList.get(i).getAssessments().isGraded();
                    if (!isGraded) {
                        JOptionPane.showMessageDialog(midPanel, "Grade is not setup yet");
                        break;
                    }
                    String type = subjectList.get(i).getAssessments().getAssessmentType();
                    String topic = subjectList.get(i).getAssessments().getTopic();
                    String format = subjectList.get(i).getAssessments().getFormat();
                    String dueDate = subjectList.get(i).getAssessments().getDueDate();
                    String grade = subjectList.get(i).getAssessments().getGrade();
                    String gradeReport = subjectName + ":" + assessmentId;
                    gradeReport += "," + type;
                    gradeReport += "," + topic;
                    gradeReport += "," + format;
                    gradeReport += "," + dueDate + "\n";
                    gradeReport += "Achievement:  " + grade + "\n";
                    gradeReport += "Knowledge:  " + GetKnowledge(grade) + "\n";
                    gradeReport += "Skill:  " + GetSkill(grade);
                    messageArea.setText(gradeReport);
                    break;
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(midPanel, e.getMessage());
        }

    }

    //Method to clear the display message display in the panel
    private void ClearDisplay() {
        txtStudentName.setText("");
        txtYear.setText("");
        messageArea.setText("");
        assessmentCounter = 0;
        cbSubjects.removeAllItems();
        cbAssessments.removeAllItems();
        cbSubjects.addItem(CB_SUBJECT_DEFAULT_TEXT);
        if (cbAchievements.getItemCount() > 0) {
            cbAchievements.removeAllItems();
            cbAchievements.addItem(CB_ACHIEVEMENT_DEFAULT_TEXT);
        }
        btnLoadAssessment.setEnabled(false);
        subjectList = new ArrayList<Subject>();
    }

    private void save() {

        new DatabaseUtility().saveDataIntoDatabase(studentList, subjectList);

    }

    //Method to exit system
    private void Exit() {
        System.exit(0);
    }

    //Create student object in text change event of year text field
    private void EnableCreate() {
        try {
            String studentName = "";
            String year = "0";
            Student obj = new Student();
            studentName = txtStudentName.getText();
            year = txtYear.getText();
            obj.setName(studentName);
            obj.setYear(year);

            if (studentName.length() > 1 && year.length() == 2 && year.matches("\\d+")) {
                btnCreateStudent.setEnabled(true);
                messageArea.setText("You can now add student using the Create Student button");
            } else if (year.length() != 2) {
                messageArea.setText("Year should be 2 digit number only");
                btnCreateStudent.setEnabled(false);
            } else {
                btnCreateStudent.setEnabled(false);
                messageArea.setText("");
            }
        } catch (Exception e) {
            txtYear.setText("");
            JOptionPane.showMessageDialog(midPanel, e.getMessage(), "System Exception", 0);
        }

    }

    //To load assessments in combo box base on the subject selected
    private void UpdateAssessments(String SubjectName) {
        try {
            cbAssessments.removeAllItems();
            cbAssessments.addItem(CB_ASSESSMENT_DEFAULT_TEXT);
            for (int i = 0; i < subjectList.size(); i++) {
                String name = subjectList.get(i).getName();
                String id = subjectList.get(i).getAssessments().getAssessmentId();
                String type = subjectList.get(i).getAssessments().getAssessmentType();
                if (name.toUpperCase().equals(SubjectName.toUpperCase())) {
                    cbAssessments.addItem(name + "," + id + "," + type);
                }
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(midPanel, ex.getMessage());
        }

    }

//To check if selected assessment have grade setup already
    private void CheckGradeSetupStatus() {
        boolean graded = false;
        String grade = "";
        String[] selectedAssessment = cbAssessments.getSelectedItem().toString().split(",");
        String subjectName = selectedAssessment[0];
        String assessmentId = selectedAssessment[1];
        for (int i = 0; i < subjectList.size(); i++) {
            String name = subjectList.get(i).getName();
            String id = subjectList.get(i).getAssessments().getAssessmentId();
            if ((subjectName.toUpperCase().equals(name.toUpperCase())) && assessmentId.equals(id)) {
                graded = subjectList.get(i).getAssessments().isGraded();
                grade = subjectList.get(i).getAssessments().getGrade();
                break;
            }
        }
        if (graded) {
            cbAchievements.setSelectedItem(grade);
        }

    }

    //Lookup skill level of corresponding grade
    private String GetKnowledge(String grade) {

        return knowledgeMap.get(grade);
    }

    //Lookup skill level of corresponding grade
    private String GetSkill(String grade) {
        return skillMap.get(grade);
    }

    // To setup the dictionary for the purpose of displaying grade
    private void InitializeMapTableForGrade() {
        knowledgeMap.put("Very high", "thorough understanding");
        knowledgeMap.put("High", "clear understanding");
        knowledgeMap.put("Sound", "understanding");
        knowledgeMap.put("Developing", "understands aspects of ");
        knowledgeMap.put("Emerging", "basic understanding");

        skillMap.put("Very high", "uses a high level of skill in both familiar and new situations");
        skillMap.put("High", "uses a high level of skill in familiar situations, and is beginning to use skills in new situations");
        skillMap.put("Sound", "uses skills in situations familiar to them");
        skillMap.put("Developing", "uses varying levels of skill in situations familiar to them");
        skillMap.put("Emerging", "beginning to use skills in familiar situations");
    }

    // Main function
    public static void main(String[] args) {
        new StudyMonitorInterfaceJava();
    }
}
