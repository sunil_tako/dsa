/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package studymonitorinterface.java;

/**
 *
 * @author Admin
 */
public class Assessment {

    /**
     * @return the Id
     */
    public int getId() {
        return Id;
    }

    /**
     * @param Id the Id to set
     */
    public void setId(int Id) {
        this.Id = Id;
    }
    private int Id;
    private String AssessmentId;
    private String AssessmentType;
    private String Topic;
    private String Format;
    private String DueDate;
    private boolean graded;

    /**
     * @return the AssessmentId
     */
    public String getAssessmentId() {
        return AssessmentId;
    }

    /**
     * @param AssessmentId the AssessmentId to set
     */
    public void setAssessmentId(String AssessmentId) {
        this.AssessmentId = AssessmentId;
    }

    /**
     * @return the AssessmentType
     */
    public String getAssessmentType() {
        return AssessmentType;
    }

    /**
     * @param AssessmentType the AssessmentType to set
     */
    public void setAssessmentType(String AssessmentType) {
        this.AssessmentType = AssessmentType;
    }

    /**
     * @return the Topic
     */
    public String getTopic() {
        return Topic;
    }

    /**
     * @param Topic the Topic to set
     */
    public void setTopic(String Topic) {
        this.Topic = Topic;
    }

    /**
     * @return the Format
     */
    public String getFormat() {
        return Format;
    }

    /**
     * @param Format the Format to set
     */
    public void setFormat(String Format) {
        this.Format = Format;
    }

    /**
     * @return the DueDate
     */
    public String getDueDate() {
        return DueDate;
    }

    /**
     * @param DueDate the DueDate to set
     */
    public void setDueDate(String DueDate) {
        this.DueDate = DueDate;
    }

    /**
     * @return the graded
     */
    public boolean isGraded() {
        return graded;
    }

    /**
     * @param graded the graded to set
     */
    public void setGraded(boolean graded) {
        this.graded = graded;
    }

}
